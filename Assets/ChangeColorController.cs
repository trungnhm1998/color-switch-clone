﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorController : MonoBehaviour {

	public SpriteRenderer m_SpriteRenderer;


	public Color[] m_Colors;
	private COLOR m_Color;
	// Use this for initialization
	void Start () {
		m_Color = (COLOR)GameStateInstance.getInstance().randomColor();
		m_SpriteRenderer.color = m_Colors[(int)m_Color];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	COLOR getColor() {
		return m_Color;
	}
}
