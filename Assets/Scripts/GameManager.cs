﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public float m_DistanceBetweenCircle;
	private Queue<GameObject> m_Circles;
	public GameObject m_CirclePrefab;

	private GameObject m_LastCircle;
	// public GameObject m_Circle;
	public Camera m_Camera;
	// Use this for initialization
	void Start () {
		m_Circles = new Queue<GameObject>();

		for (var i = 0; i < 3; i++) {

			var tempCircle = Instantiate(m_CirclePrefab, new Vector3(0.0f, i *  m_DistanceBetweenCircle, 0.0f), Quaternion.identity);
			m_Circles.Enqueue(tempCircle);
			m_LastCircle = tempCircle;
		}
	}
	
	// Update is called once per frame
	void Update () {
		checkCircles();

		if (GameStateInstance.getInstance().m_Won == false) {
			while (m_Circles.Count > 0) {
				m_Circles.Dequeue().GetComponent<CircleController>().enabled = false;
			}
		}
	}

	void checkCircles() {
		if (m_Circles.Count != 0) {
			if (m_Circles.Peek().transform.position.y < m_Camera.transform.position.y - 6.25) {
				var CircleToBeDestroy = m_Circles.Dequeue();
				Destroy(CircleToBeDestroy);
				m_LastCircle = spawnCircle();
				m_Circles.Enqueue(m_LastCircle);
			}
		}
	}

	GameObject spawnCircle() {
		return Instantiate(m_CirclePrefab,
												new Vector3(0.0f,
																		m_LastCircle.transform.position.y + m_DistanceBetweenCircle,
																		0.0f),
												Quaternion.identity);
		}
}
