﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController : MonoBehaviour {

	public float m_RotateSpeed;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0.0f, 0.0f, m_RotateSpeed * Time.deltaTime);
	}
}
