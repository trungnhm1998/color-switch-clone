﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject m_Player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (m_Player.transform.position.y > transform.position.y)
			transform.position = new Vector3(transform.position.x, m_Player.transform.position.y, transform.position.z);
	}
}
