﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {
	
	public float m_Gravity;
	// force jump
	public float m_Force;
	public Rigidbody2D m_RB;
	public SpriteRenderer m_SpriteRenderer;

	public Color[] m_Colors;

	private COLOR m_Color;
	private string[] m_StrColors = new string[] {"Red", "Blue", "Magenta", "Yellow"};
	// Use this for initialization
	void Start () {
		m_RB.gravityScale = m_Gravity;
		this.setBallColor((COLOR)GameStateInstance.getInstance().randomColor());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0) || Input.GetKeyDown(KeyCode.Space)) {
			// Debug.Log("Key down");
			m_RB.velocity = new Vector2(m_RB.velocity.x, m_Force * Time.deltaTime);
		}
		
	}

	void setBallColor(COLOR color) {
		m_SpriteRenderer.color = m_Colors[(int) color];
		m_Color = color;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
			if (other.tag != m_StrColors[(int)m_Color]) {
				Debug.Log("GAME OVER");
				Destroy(m_RB);
				GameStateInstance.getInstance().m_Won = false;
			}
			// enabled = false;
	}
}
