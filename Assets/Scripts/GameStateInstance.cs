﻿using UnityEngine;
public class GameStateInstance {

	private static GameStateInstance _Instance;
	public bool m_Won;
	Color m_Color;
	GameStateInstance() {
		m_Won = true;
	}

	public int randomColor() {
		return Random.Range(0, 4);
	}

	// public void Init() {
	// 	m_Won = true;
	// }

	public static GameStateInstance getInstance() {
		if (_Instance == null) {
			_Instance = new GameStateInstance();
		}

		return _Instance;
	}
}
